import { Component } from '@angular/core';
import { ModalController, PopoverController, NavController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  Palabra = "";
  constructor(private nav: NavController, public modalController: ModalController, private popover : PopoverController){
  }

  async MostrarModal(myModel){
    // console.log(this.Palabra);
    const popOver = await this.popover.create({
      component : ModalPage,
      componentProps: {
        Palabra : this.Palabra
      },
      event : myModel
    });
    popOver.present();
  }

  // myArray : any = [{
  //   name : 'joe',
  //   edad : 15,
  //   // color : "primary"

  // },
  // {
  //   name : 'juana',
  //   edad : 10,
  //   // color : "danger"
  // }];

  // constructor() {
  //   let data = {
  //     name : 'jesus',
  //     edad : 50,
  //     // color : "success"
  //   };

  //   this.myArray.push(data)
  // }
  // calcularcolor( edad:number){
  //   if (edad > 11)
  //     return 'danger';
  //   return 'primary';
  // }
}
