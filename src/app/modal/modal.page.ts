import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit{
  Palabra = "";
  constructor(private nav : NavParams, private pop : PopoverController) { }

  ngOnInit() {
    this.Palabra = this.nav.get('Palabra');
    console.log(this.Palabra);
  }

}
